<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html >

<head>
    <meta charset="UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu-page.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin-ext" rel="stylesheet">
    <title>Mój profil</title>
</head>
<header>
    <div class="logo">
        <img src="${pageContext.request.contextPath}/resources/css/img/logociemne.png" alt="" width="350" height="350">
    </div>
</header>

<body>
	<c:url var="userTransactionsLink" value="/user/userPage">
        <%--TODO -PRZEKAZAC ODPOWIEDNIE PARAMETRY--%>
	
    <container>
        <h1>Witaj, ${loggedUser.login} !</h1>
        <main>
            <div class="clearfix">
                <div class="1">
                    <a href=${userTransactionsLink}>
                        <button>
                          dodaj wydatek
                        </button>
                    </a>
                </div>
                <div class="2">
                    <button>
                        statystyki
                    </button>
                </div>
                <div class="3">
                    <button>
                        konfiguracja
                    </button>
                </div>
                <div class="4">
                    <button>
                        faq
                    </button>
                </div>
                <div class="5">
                    <button>
                        wyloguj się

                    </button>
                </div>
            </div>
        </main>
    </container>
</body>

</html>

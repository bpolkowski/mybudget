<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/start-page-style.css">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin-ext" rel="stylesheet">
    <title>Moneytoring - budget manager</title>
</head>


    <header>
      <div class="logo">
            <img src="${pageContext.request.contextPath}/resources/css/img/logociemne.png" alt="">
  </div>
    </header>

    <body>
    <c:url var="showAddUserForm" value="/user/showAddUserForm"/>
    <c:url var="loginPage" value="/login/loginPage"/>
        <section class="buttons">
            <div class="register">
                <a href="${showAddUserForm}"><i class="fas fa-user"></i>Rejestracja</a>
            </div>
            <div class="login">
                <a href="${loginPage}">Zaloguj się</a>
            </div>
        </section>
    </body>


    <footer></footer>
</html>

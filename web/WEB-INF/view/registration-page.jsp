<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/registration-page-style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin-ext" rel="stylesheet">

    <title>Register</title>
</head>

<header>
    <div class="logo">
        <img src="${pageContext.request.contextPath}/resources/css/img/logociemne.png" alt="" width="250" height="250">
    </div>
</header>

<body>
<div>
    <p>Formularz rejestracyjny:</p>
</div>

<div class="content">

    <form:form action="saveUser" modelAttribute="newUser" method="post" autocomplete="off">

        <div class="row">
            <label for="email">Adres e-mail</label>
            <form:input path="email" id="email" type="email" required = "true"/>
        </div>

        <div class="row">
            <label for="login">Login</label>
            <form:input path="login" id="name" type="text" required = "true"/>
        </div>

        <div class="row">
            <label for="password">Hasło</label>
            <form:input path="password" id="password" type="password" required = "true"/>
        </div>

        <div class="row">
            <input type="submit" id="submit" value="Prześlij" required="true"/>
        </div>

    </form:form>


</div>


</body>


<footer></footer>

</html>

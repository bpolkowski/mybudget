<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>
<head>
    <title>User page</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/test.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/user-page-style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin-ext" rel="stylesheet">
</head>
<body>
    <div class="mainPage">
        <c:url var="addTransactionLink" value="/transaction/addTransaction">
        <c:param name="userId" value="${loggedUser.id}"/></c:url>
        <c:url var="addFixedTransactionLink" value="/fixedTransaction/addFixedTransaction">
        <c:param name="userId" value="${loggedUser.id}"/></c:url>
        <div class="container-fluid">
            <div class="jumbotron text-center">
                <h1 id="header-text">Welcome ${loggedUser.login} !</h1>
                <p id="welcome-text">Here you can find your latest transactions!</p>
            </div>
            <div class="data">
                <div class="col-sm-5" >
                    <div class="month-filter">
                        <form:form action="/user/filterByMonth">
                            Sort by month:
                            <select id="monthName" name="monthName">
                                <c:forEach items="${userTransactionsPerMonth}" var="month">
                                    <option value="${month.key}">${month.key}</option>
                                </c:forEach>
                                <input type="submit" class="btn" value="Filter"/>
                            </select>
                        </form:form>
                    </div>
                    <div>
                        <a href="${addTransactionLink}">
                            <input type="submit" class="btn" value="Add Transaction"/>
                        </a>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Description</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:if test="${transactionsInMonth != null}">
                            <c:forEach items="${transactionsInMonth}" var="transaction">
                                <c:url var="deleteTransactionLink" value="/transaction/deleteTransaction">
                                    <c:param name="transactionId" value="${transaction.transactionId}"/>
                                    <c:param name="userId" value="${loggedUser.id}"/>
                                </c:url>
                                <c:url var="updateTransactionLink" value="/transaction/updateTransaction">
                                    <c:param name="transactionId" value="${transaction.transactionId}"/>
                                    <c:param name="userId" value="${loggedUser.id}"/>
                                </c:url>
                                <tr>
                                    <td>${transaction.category.categoryName}</td>
                                    <td>${transaction.moneyAmount}</td>
                                    <td>${transaction.description}</td>
                                    <td>${transaction.transactionDate}</td>
                                    <td><a href="${deleteTransactionLink}">Delete</a> </td>
                                    <td><a href="${updateTransactionLink}">Update</a> </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        <c:if test="${transactionsInMonth == null}">
                            <tr>
                                <c:forEach var="transaction" items="${userTransactions}">
                                    <c:url var="deleteTransactionLink" value="/transaction/deleteTransaction">
                                        <c:param name="transactionId" value="${transaction.transactionId}"/>
                                        <c:param name="userId" value="${loggedUser.id}"/>
                                    </c:url>
                                    <c:url var="updateTransactionLink" value="/transaction/updateTransaction">
                                        <c:param name="transactionId" value="${transaction.transactionId}"/>
                                        <c:param name="userId" value="${loggedUser.id}"/>
                                    </c:url>
                                    <tr>
                                        <td>${transaction.category.categoryName}</td>
                                        <td>${transaction.moneyAmount}</td>
                                        <td>${transaction.description}</td>
                                        <td>${transaction.transactionDate}</td>
                                        <td><a href="${deleteTransactionLink}">Delete</a> </td>
                                        <td><a href="${updateTransactionLink}">Update</a> </td>
                                    </tr>
                                </c:forEach>
                            </tr>
                        </c:if>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <div class="fixed-month-filter">
                        <form:form action="/user/filterFixedByMonth">
                            Sort fixed transactions by month:
                            <select name="fixedMonthName">
                                <c:forEach items="${userFixedTransactionsPerMonth}" var="month">
                                    <option value="${month.key}">${month.key}</option>
                                </c:forEach>
                                <input type="submit" class="btn" value="Filter"/>
                            </select>
                        </form:form>
                    </div>
                    <div>
                        <a href="${addFixedTransactionLink}">
                            <input type="submit" class="btn" value="Add Fixed Transaction"/>
                        </a>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Begin date</th>
                            <th>End date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${fixedTransactionsInMonth == null}">
                            <tr>
                            <c:forEach var="fixedTransaction" items="${userFixedTransactions}">
                                <c:url var="deleteTransactionLink" value="/fixedTransaction/deleteFixedTransaction">
                                    <c:param name="fixedTransactionId" value="${fixedTransaction.fixedTransactionId}"/>
                                    <c:param name="fixedUserId" value="${loggedUser.id}"/>
                                </c:url>
                                <c:url var="updateTransactionLink" value="/fixedTransaction/updateFixedTransaction">
                                    <c:param name="fixedTransactionId" value="${fixedTransaction.fixedTransactionId}"/>
                                    <c:param name="fixedUserId" value="${loggedUser.id}"/>
                                </c:url>
                                <tr>
                                    <td>${fixedTransaction.category.categoryName}</td>
                                    <td>${fixedTransaction.fixedMoneyAmount}</td>
                                    <td>${fixedTransaction.transactionComment}</td>
                                    <td>${fixedTransaction.durationBeginDate}</td>
                                    <td>${fixedTransaction.durationEndDate}</td>
                                    <td><a href="${deleteTransactionLink}">Delete</a> </td>
                                    <td><a href="${updateTransactionLink}">Update</a> </td>
                                </tr>
                            </c:forEach>
                            </tr>
                        </c:if>
                        <c:if test="${fixedTransactionsInMonth != null}">
                            <c:forEach items="${fixedTransactionsInMonth}" var="month">
                                <c:url var="deleteTransactionLink" value="/fixedTransaction/deleteFixedTransaction">
                                    <c:param name="fixedTransactionId" value="${month.fixedTransactionId}"/>
                                    <c:param name="fixedUserId" value="${loggedUser.id}"/>
                                </c:url>
                                <c:url var="updateTransactionLink" value="/fixedTransaction/updateFixedTransaction">
                                    <c:param name="fixedTransactionId" value="${month.fixedTransactionId}"/>
                                    <c:param name="fixedUserId" value="${loggedUser.id}"/>
                                </c:url>
                                <tr>
                                    <td>${month.category.categoryName}</td>
                                    <td>${month.fixedMoneyAmount}</td>
                                    <td>${month.transactionComment}</td>
                                    <td>${month.durationBeginDate}</td>
                                    <td>${month.durationEndDate}</td>
                                    <td><a href="${deleteTransactionLink}">Delete</a> </td>
                                    <td><a href="${updateTransactionLink}">Update</a> </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

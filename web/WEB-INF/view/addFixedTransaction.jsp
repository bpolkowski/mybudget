<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Fixed Transaction Form</title>


</head>
<body>

<h1>Add fixed-transaction to database:</h1>

<form:form action="saveFixedTransaction" modelAttribute="newFixedTransaction" method="post">
    <form:hidden path="fixedTransactionId"/>
    <form:hidden path="fixedTransactionUser.id"/>
    <table>

        <tr>
                <td><label>Choose category:</label></td>
                <td>
                    <form:select name="fixedTransactionsInCategory.id" path="category.id">
                        <c:forEach items="${fixedTransactionsInCategory}" var="category">
                            <form:option value="${category.id}">${category.categoryName}
                            </form:option>
                        </c:forEach>
                    </form:select>
                </td>
        </tr>

        <tr>
        </tr>
        <tr>
            <td><label>Amount:</label></td>
            <td><form:input path="fixedMoneyAmount" />
                <form:errors path="fixedMoneyAmount" cssClass="error"/>
            </td>
        </tr>

        <tr>
            <td><label>Fixed transaction start date:</label></td>
            <td><form:input type = "date" path="durationBeginDate"/>  <%--pattern="dd-MM-yyyy"--%>
            </td>
        </tr>

        <tr>
            <td><label>Fixed transaction end date:</label></td>
            <td><form:input type = "date" path="durationEndDate"/>  <%--pattern="dd-MM-yyyy"--%>
            </td>
        </tr>

        <tr>
            <td><label>Description:</label></td>
            <td><form:input path="transactionComment" />
            </td>
        </tr>

        <tr>
            <label></label>
            <td><input type="submit" value="Submit" class="save"/>
            </td>
        </tr>
    </table>
</form:form>

</body>
</html>

<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Transaction Form</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/add-transaction-style.css">
</head>
<body>
    <div class="mainPage">

        <form:form action="saveTransaction" modelAttribute="newTransaction" method="post">
            <form:hidden path="transactionId"/>
            <form:hidden path="user.id"/>
        <div class="table-container">
                <h1>Add transaction to database:</h1>
            <table>
                <tr>
                    <td><label>Choose category:</label></td>
                    <td><form:select name="category.id" path="category.id" >
                        <c:forEach items="${category}" var="category">
                            <form:option value="${category.id}">${category.categoryName}
                            </form:option>
                        </c:forEach>
                    </form:select>
                </tr>
                        <tr>
                            <td><label>Amount:</label></td>
                            <td><form:input path="moneyAmount" />
                            </td>
                        </tr>

                        <tr>
                            <td><label>Transaction date:</label></td>
                            <td><form:input type = "date" path="transactionDate" />
                            </td>
                        </tr>

                        <tr>
                            <td><label>Description:</label></td>
                            <td><form:input path="description"/>
                            </td>
                        </tr>
                        <tr class="btn-position">
                            <label></label>
                            <td><input type="submit" class="btn" value="Submit"/>
                            </td>
                        </tr>
            </table>
        </div>
        </form:form>

    </div>
</body>
</html>

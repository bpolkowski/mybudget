<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>

<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login-page-style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin-ext" rel="stylesheet">

    <title>Login</title>
</head>

<header>
    <div class="logo">
        <img src="${pageContext.request.contextPath}/resources/css/img/logociemne.png" alt="" width="250" height="250">
    </div>
</header>

<body>
<div>
    <p>Zaloguj się do Moneytoring</p>
</div>

<div class="content">
    <form:form action="processLogin" modelAttribute="user" method="post" autocomplete="off">

        <div class="row">
            <label for="login">Login</label>
            <form:input path="login" id="name" type="text" required = "true"/>
        </div>

        <div class="row">
            <label for="password">Hasło</label>
            <form:input path="password" id="password" type="password" required = "true"/>
        </div>

        <input type="submit" value="Prześlij"/>

    </form:form>


</div>


</body>


<footer></footer>

</html>

package com.mybudget.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.mybudget.services","com.mybudget.utils"})
public class RootConfig {}


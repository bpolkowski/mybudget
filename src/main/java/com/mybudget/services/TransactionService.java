package com.mybudget.services;

import com.mybudget.entity.Transaction;

import java.util.Date;
import java.util.List;

public interface TransactionService {
    List<Transaction> getAllTransactions();
    Transaction getSingleTransactionById(int idToFind);
    Transaction getTransactionByDate(Date dateToFind);
    List<Transaction> getUserTransactionsById(int userId);
    void deleteTransactionById(int idToDelete);
    void saveTransaction(Transaction transactionToSave);
}

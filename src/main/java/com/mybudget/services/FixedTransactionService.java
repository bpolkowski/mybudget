package com.mybudget.services;

import com.mybudget.entity.FixedTransaction;

import java.util.Date;
import java.util.List;


public interface FixedTransactionService {

    List<FixedTransaction> getAllFixedTransactions();
    FixedTransaction getSingleFixedTransactionById(int idToFind);
    List<FixedTransaction> getTransactionsByBeginDate(Date beginDateToFind);
    List<FixedTransaction> getTransactionsByEndDate(Date endDateToFind);
    List<FixedTransaction> getUserFixedTransactionsById(int userId);
    void deleteFixedTransactionById(int idTransaction);
    void saveFixedTransaction(FixedTransaction transactionToSave);

}

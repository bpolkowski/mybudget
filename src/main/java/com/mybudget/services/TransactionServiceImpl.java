package com.mybudget.services;

import com.mybudget.entity.Transaction;
import com.mybudget.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService{

    @Autowired
    private TransactionRepository transactionRepository;

    public List<Transaction> getAllTransactions() {
        return transactionRepository.getTransactionsList();
    }
    public void deleteTransactionById(int idToDelete) {
        transactionRepository.deleteTransactionById(idToDelete);
    }
    public Transaction getSingleTransactionById(int idToFind) { return transactionRepository.getSingleTransactionById(idToFind); }
    public Transaction getTransactionByDate(Date dateToFind) {return transactionRepository.getTransactionByDate(dateToFind);}
    public List<Transaction> getUserTransactionsById(int userId) { return transactionRepository.getUserTransactionsById(userId);}
    public void saveTransaction(Transaction transactionToSave) { transactionRepository.saveTransaction(transactionToSave);}
}

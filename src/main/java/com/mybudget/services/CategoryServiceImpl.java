package com.mybudget.services;

import com.mybudget.entity.Category;
import com.mybudget.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryRepository categoryRepository;

    public Category getCategoryById(int categoryId) {
        return categoryRepository.getCategoryById(categoryId);
    }
    public List<Category> getAllCategories() {
        return categoryRepository.getAllCategories();
    }
    public List<Category> getAllExpenditureCategories() {
        return categoryRepository.getAllExpenditureCategories();
    }
    public void deleteCategory(int categoryToDeleteId) {
        categoryRepository.deleteCategory(categoryToDeleteId);
    }
    public void saveCategory(Category categoryToSave) {
        categoryRepository.saveCategory(categoryToSave);
    }
}

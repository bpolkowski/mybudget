package com.mybudget.services;

import com.mybudget.entity.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User getUserById(int userId);
    User getUserByLoginAndPassword(String login, String password);
    void deleteUser(int idToDelete);
    void saveUser(User userToSave);
}

package com.mybudget.services;

import com.mybudget.entity.User;
import com.mybudget.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;


    public List<User> getAllUsers() {return userRepository.getAllUsers(); }
    public User getUserById(int userId) { return userRepository.getUserById(userId); }
    public User getUserByLoginAndPassword(String login, String password) { return userRepository.getUserByLoginAndPassword(login,password); }
    public void deleteUser(int idToDelete) { userRepository.deleteUser(idToDelete); }
    public void saveUser(User userToSave) { userRepository.saveUser(userToSave); }
}

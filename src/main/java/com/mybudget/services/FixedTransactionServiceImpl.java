package com.mybudget.services;

import com.mybudget.entity.FixedTransaction;
import com.mybudget.repositories.FixedTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class FixedTransactionServiceImpl implements FixedTransactionService{

    @Autowired
    private FixedTransactionRepository fixedTransactionRepository;

    public List<FixedTransaction> getAllFixedTransactions() { return fixedTransactionRepository.getAllFixedTransactions(); }
    public FixedTransaction getSingleFixedTransactionById(int idToFind) { return fixedTransactionRepository.getSingleFixedTransactionById(idToFind); }
    public List<FixedTransaction> getUserFixedTransactionsById(int userId) { return fixedTransactionRepository.getUserFixedTransactionsById(userId);}
    public List<FixedTransaction> getTransactionsByBeginDate(Date beginDateToFind) { return fixedTransactionRepository.getFixedTransactionsByBeginDate(beginDateToFind); }
    public List<FixedTransaction> getTransactionsByEndDate(Date endDateToFind) { return fixedTransactionRepository.getFixedTransactionsByEndDate(endDateToFind); }
    public void deleteFixedTransactionById(int idTransaction) { fixedTransactionRepository.deleteFixedTransactionById(idTransaction); }
    public void saveFixedTransaction(FixedTransaction transactionToSave) { fixedTransactionRepository.saveFixedTransaction(transactionToSave); }
}

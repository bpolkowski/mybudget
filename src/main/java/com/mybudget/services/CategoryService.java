package com.mybudget.services;

import com.mybudget.entity.Category;

import java.util.List;

public interface CategoryService {
    Category getCategoryById(int categoryId);
    List<Category> getAllCategories();
    List<Category> getAllExpenditureCategories();
    void deleteCategory(int categoryToDeleteId);
    void saveCategory(Category categoryToSave);

}

package com.mybudget.repositories;

import com.mybudget.entity.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CategoryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Category> getAllCategories(){
        return entityManager.createNamedQuery("get_all_categories").getResultList();
    }

    public List<Category> getAllExpenditureCategories(){
        return entityManager.createNamedQuery("get_all_expenditure_categories").getResultList();
    }

    public Category getCategoryById(int categoryId){
        return entityManager.find(Category.class, categoryId);
    }

    public void deleteCategory(int categoryId){
        Category categoryToDelete = entityManager.find(Category.class,categoryId);
        entityManager.remove(categoryToDelete);
    }

    public void saveCategory(Category category){
        if(category.getId() == 0){
            entityManager.persist(category);
        }else {
            entityManager.merge(category);
        }
    }


}

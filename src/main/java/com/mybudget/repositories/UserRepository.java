package com.mybudget.repositories;

import com.mybudget.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<User> getAllUsers(){
        return entityManager.createNamedQuery("get_all_users").getResultList();
    }

    public User getUserById(int userId){
        return entityManager.find(User.class, userId);
    }

    public User getUserByLoginAndPassword(String userLogin, String userPassword){
        return entityManager.createQuery("Select u from User u where u.login=:login and u.password=:password", User.class)
                .setParameter("login",userLogin).setParameter("password",userPassword).getSingleResult();
    }

    public void deleteUser(int userIdToDelete){
        User userToDelete = entityManager.find(User.class, userIdToDelete);
        entityManager.remove(userToDelete);
    }

    public void saveUser(User userToSave){
        if(userToSave.getId() == 0){
            entityManager.createNativeQuery("INSERT INTO user_roles (login, role) VALUES (?,?)")
                    .setParameter(1, userToSave.getLogin())
                    .setParameter(2, User.ROLE)
                    .executeUpdate();
            entityManager.persist(userToSave);
        }else{
            entityManager.createNativeQuery("INSERT INTO user_roles (login, role) VALUES (?,?)")
                    .setParameter(1, userToSave.getLogin())
                    .setParameter(2, User.ROLE)
                    .executeUpdate();
            entityManager.merge(userToSave);
        }
    }
}
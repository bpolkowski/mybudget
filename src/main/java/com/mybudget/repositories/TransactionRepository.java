package com.mybudget.repositories;

import com.mybudget.entity.Transaction;

import com.mybudget.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@Repository
public class TransactionRepository {

@Autowired
TransactionService transactionService;

    @PersistenceContext
    private EntityManager entityManager;

    public List<Transaction> getTransactionsList(){
        return entityManager.createNamedQuery("get_all_transactions").getResultList();
    }

    public List<Transaction> getUserTransactionsById(int userId){
        return entityManager.createQuery("select t from Transaction t where t.user.id=:userId", Transaction.class)
                .setParameter("userId",userId).getResultList();
    }

    public Transaction getSingleTransactionById(int transactionIdToFind){
        return entityManager.find(Transaction.class, transactionIdToFind);
    }

    public Transaction getTransactionByDate(Date dateToFind){
        return entityManager.createQuery(
                "select t from Transaction t where t.transactionDate=:date",Transaction.class)
                .setParameter("date",dateToFind).getSingleResult();
    }

    public void deleteTransactionById(int transactionToDeleteId){
        Transaction transactionToDelete = getSingleTransactionById(transactionToDeleteId);
        entityManager.remove(transactionToDelete);
    }

    public void saveTransaction(Transaction transactionToSave){
        if(transactionToSave.getTransactionId() == 0){
            entityManager.persist(transactionToSave);
        }else {
            entityManager.merge(transactionToSave);
        }
    }

}

package com.mybudget.repositories;

import com.mybudget.entity.FixedTransaction;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@Repository
public class FixedTransactionRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<FixedTransaction> getAllFixedTransactions(){
        return entityManager.createNamedQuery("get_all_fixed_transactions").getResultList();
    }

    public FixedTransaction getSingleFixedTransactionById(int idToFind){
        return entityManager.find(FixedTransaction.class, idToFind);
    }

    public List<FixedTransaction> getUserFixedTransactionsById(int userId){
        return entityManager.createQuery("select ft from FixedTransaction ft where ft.fixedTransactionUser.id=:userId",FixedTransaction.class)
                .setParameter("userId", userId).getResultList();
    }

    public List<FixedTransaction> getFixedTransactionsByBeginDate(Date beginDateToFind){
        return entityManager.createQuery(
                "select ft from FixedTransaction ft where ft.durationBeginDate=:beginDate", FixedTransaction.class)
                .setParameter("beginDate", beginDateToFind).getResultList();
    }

    public List<FixedTransaction> getFixedTransactionsByEndDate(Date endDateToFind){
        return entityManager.createQuery(
                "select ft from FixedTransaction ft where ft.durationEndDate=:endDate", FixedTransaction.class)
                .setParameter("endDate", endDateToFind).getResultList();
    }

    public void deleteFixedTransactionById(int idTransaction){
        FixedTransaction transactionToDelete = getSingleFixedTransactionById(idTransaction);
        entityManager.remove(transactionToDelete);
    }

    public void saveFixedTransaction(FixedTransaction transactionToSave){
        if (transactionToSave.getFixedTransactionId() == 0){
            entityManager.persist(transactionToSave);
        }else{
            entityManager.merge(transactionToSave);
        }
    }

}

package com.mybudget.controllers;

import com.mybudget.entity.FixedTransaction;
import com.mybudget.entity.Transaction;
import com.mybudget.entity.User;
import com.mybudget.utils.TransactionsConverter;
import com.mybudget.services.FixedTransactionService;
import com.mybudget.services.TransactionService;
import com.mybudget.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


@Controller
@RequestMapping("/user")
@SessionAttributes({"loggedUser","userTransactions","userFixedTransactions","transactionsInMonth"})
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private FixedTransactionService fixedTransactionService;
    @Autowired
    TransactionsConverter transactionConverter;

    @GetMapping("/userPage")
    public String getUserPage(Model theModel,
                              @ModelAttribute("loggedUser") User loggedUser,
                              BindingResult bindingResult) {

        List<Transaction> transactionsFromDatabase = transactionService.getUserTransactionsById(loggedUser.getId());
        List<FixedTransaction> fixedTransactionsFromDatabase = fixedTransactionService.getUserFixedTransactionsById(loggedUser.getId());
        transactionsFromDatabase.sort((o1, o2) -> o2.getTransactionDate().compareTo(o1.getTransactionDate()));
        HashMap<String, List<Transaction>> userTransactionsPerMonth = transactionConverter.convertTransactionsPerMonth(loggedUser.getId());
        HashMap<String, List<FixedTransaction>> userFixedTransactionsPerMonth = transactionConverter.convertFixedTransactionsPerMonth(loggedUser.getId());

        theModel.addAttribute("userTransactionsPerMonth", userTransactionsPerMonth);
        theModel.addAttribute("userFixedTransactionsPerMonth", userFixedTransactionsPerMonth);
        theModel.addAttribute("userTransactions",transactionsFromDatabase);
        theModel.addAttribute("userFixedTransactions",fixedTransactionsFromDatabase);
        theModel.addAttribute("loggedUser", loggedUser);
        return "user-page";
    }

    @GetMapping("/showAddUserForm")
    public String showAddUserFormular(Model theModel){
        User newUser = new User();
        theModel.addAttribute("newUser", newUser);
        return "registration-page";
    }

    @PostMapping("/saveUser")
    public String saveUser(@ModelAttribute("newUser") User newUser, BindingResult bindingResult){
        Calendar calendar = Calendar.getInstance();
        Timestamp currentTimestamp = new Timestamp(calendar.getTime().getTime());
        newUser.setUserJoinDate(currentTimestamp);
        userService.saveUser(newUser);
        return "redirect:/login/loginPage";
    }

    @GetMapping("/showDeleteForm")
    public String deleteUser(@RequestParam("userId") int idFromUserToDelete, Model theModel){
        userService.deleteUser(idFromUserToDelete);
        return "redirect:/user/usersList";
    }

    @GetMapping("/showUpdateUserForm")
    public String updateUser(@RequestParam("userId") int idFromUserToUpdate, Model theModel){
        User userToUpdate = userService.getUserById(idFromUserToUpdate);
        theModel.addAttribute("newUser", userToUpdate);
        return "user-add-form";
    }

    @PostMapping("/filterByMonth")
    public String filterByMonth(@RequestParam("monthName") String monthChoosen,
                                @ModelAttribute("loggedUser") User loggedUser,
                                Model theModel,
                                RedirectAttributes redirectAttributes){
        List<Transaction> monthlyTransactions = transactionConverter.transactionsInMonth(monthChoosen);
        theModel.addAttribute("transactionsInMonth", monthlyTransactions);
        redirectAttributes.addFlashAttribute("transactionsInMonth", monthlyTransactions);
        return "redirect:/user/userPage";
    }

    @PostMapping("/filterFixedByMonth")
    public String filterFixedByMonth(@RequestParam("fixedMonthName") String monthChoosen,
                                @ModelAttribute("loggedUser") User loggedUser,
                                Model theModel,
                                RedirectAttributes redirectAttributes){
        List<FixedTransaction> monthlyFixedTransactions = transactionConverter.fixedTransactionsInMonth(monthChoosen);
        theModel.addAttribute("fixedTransactionsInMonth", monthlyFixedTransactions);
        redirectAttributes.addFlashAttribute("fixedTransactionsInMonth", monthlyFixedTransactions);
        return "redirect:/user/userPage";
    }
}

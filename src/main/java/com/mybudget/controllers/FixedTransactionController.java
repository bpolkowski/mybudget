package com.mybudget.controllers;

import com.mybudget.entity.FixedTransaction;
import com.mybudget.services.CategoryService;
import com.mybudget.services.FixedTransactionService;
import com.mybudget.services.UserService;
import com.mybudget.utils.UserInDatabaseValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/fixedTransaction")
public class FixedTransactionController {

    @Autowired
    FixedTransactionService fixedTransactionService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    UserService userService;

    @Autowired
    UserInDatabaseValidator validator;

    @GetMapping("/addFixedTransaction")
    public String fixedTransactionList(Model theModel, @RequestParam("userId") int loggedUserId){
        FixedTransaction fixedTransaction = new FixedTransaction();
        fixedTransaction.setFixedTransactionUser(validator.getUserEntityFromDatabase(userService.getUserById(loggedUserId)));
        System.out.println(userService.getUserById(loggedUserId));
        theModel.addAttribute("fixedTransactionsInCategory",categoryService.getAllCategories());
        theModel.addAttribute("newFixedTransaction", fixedTransaction);
        return "addFixedTransaction";
    }

    @PostMapping("/saveFixedTransaction")
    public String saveFixedTransaction(@ModelAttribute("newFixedTransaction") FixedTransaction newFixedTransaction,
                                       BindingResult theBindingResult, HttpServletRequest request,
                                       RedirectAttributes redirectAttributes) throws ParseException {
        DateFormat formatDate1 = new SimpleDateFormat("yyyy-MM-dd");
        Date formatedDate1 = formatDate1.parse(request.getParameter("durationBeginDate"));
        Date formatedDate2 = formatDate1.parse(request.getParameter("durationEndDate"));
        newFixedTransaction.setDurationBeginDate(formatedDate1);
        newFixedTransaction.setDurationEndDate(formatedDate2);
        fixedTransactionService.saveFixedTransaction(newFixedTransaction);

        redirectAttributes.addFlashAttribute("loggedUser", userService.getUserById(newFixedTransaction.getFixedTransactionUser().getId()));
            return "redirect:/user/userPage";
        }

    @GetMapping("/deleteFixedTransaction")
    public String deleteFixedUser(@RequestParam("fixedTransactionId") int idFromTransactionToDelete,
                                  @RequestParam("fixedUserId") int loggedUserId,
                                  RedirectAttributes redirectAttributes){
        fixedTransactionService.deleteFixedTransactionById(idFromTransactionToDelete);
        redirectAttributes.addFlashAttribute("loggedUser", userService.getUserById(loggedUserId));
        return "redirect:/user/userPage";
    }

    @GetMapping("/updateFixedTransaction")
    public String updateFixedUser(@RequestParam("fixedTransactionId") int idFromTransactionToUpdate,
                                  @RequestParam("fixedUserId") int loggedUserId,
                                  RedirectAttributes redirectAttributes,
                                  Model theModel){
        FixedTransaction fixedTransactionToUpdate = fixedTransactionService.getSingleFixedTransactionById(idFromTransactionToUpdate);
        fixedTransactionToUpdate.setFixedTransactionUser(userService.getUserById(loggedUserId));
        theModel.addAttribute("newFixedTransaction",fixedTransactionToUpdate);
        theModel.addAttribute("fixedTransactionsInCategory", categoryService.getAllCategories());
        redirectAttributes.addFlashAttribute("userId", loggedUserId);
        return "addFixedTransaction";
    }
}



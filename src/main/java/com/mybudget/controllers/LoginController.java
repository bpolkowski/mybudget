package com.mybudget.controllers;

import com.mybudget.entity.User;
import com.mybudget.utils.TransactionsConverter;
import com.mybudget.services.UserService;
import com.mybudget.utils.UserInDatabaseValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    UserInDatabaseValidator userValidator;

    @Autowired
    TransactionsConverter transactionsConverter;

    @GetMapping("/startPage")
    public String showStartPage(Model theModel){
        return "start-page";
    }


    @GetMapping("/loginPage")
    public String showLoginPage(Model theModel){
        User userAttemptingToLogIn = new User();
        theModel.addAttribute("user", userAttemptingToLogIn);
        return "login-page";
    }

    @PostMapping("/processLogin")
    public String processLoginPage(@ModelAttribute("user") User userToLogIn,
                                   RedirectAttributes redirectAttributes){
        if(userValidator.isUserInDatabase(userToLogIn)){
            redirectAttributes.addFlashAttribute("loggedUser",userValidator.getUserEntityFromDatabase(userToLogIn));
            return "redirect:/user/userPage";
        }else {
            return "redirect:/login/loginPage";
        }
    }
}

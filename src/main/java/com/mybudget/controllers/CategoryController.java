package com.mybudget.controllers;

import com.mybudget.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/category")
public class CategoryController {

   @Autowired
    private CategoryService categoryService;

    @GetMapping("/list")
    public String categoriesList(Model theModel){
        theModel.addAttribute("allCategoriesList" , categoryService.getAllCategories());
        return "test-main-page";
    }
}

package com.mybudget.controllers;

import com.mybudget.entity.Category;
import com.mybudget.entity.FixedTransaction;
import com.mybudget.entity.Transaction;
import com.mybudget.entity.User;
import com.mybudget.services.CategoryService;
import com.mybudget.services.FixedTransactionService;
import com.mybudget.services.TransactionService;
import com.mybudget.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    FixedTransactionService fixedTransactionService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    UserService userService;

    //@ControllerAdvice

    /*@ModelAttribute("categories")
    public List<Category> allCategories() {
        return categoryService.getAllCategories();
    }*/

    @GetMapping("/addTransaction")
    public String transactionsList(Model theModel, @RequestParam("userId") int loggedUserId,
                                   RedirectAttributes redirectAttributes){
        Transaction transaction = new Transaction();
        transaction.setUser(userService.getUserById(loggedUserId));
        theModel.addAttribute("user",userService.getUserById(loggedUserId));
        theModel.addAttribute("category", categoryService.getAllCategories());
        theModel.addAttribute("newTransaction", transaction);
        return "addTransaction";
    }

    @PostMapping("/saveTransaction")
    public String saveTransaction(@ModelAttribute("newTransaction") Transaction newTransaction,
                                  BindingResult theBindingResult,
                                  HttpServletRequest request,
                                  RedirectAttributes redirectAttributes) throws ParseException {
        Date formatedDate = getFormattedDate(request);
        newTransaction.setTransactionDate(formatedDate);
        transactionService.saveTransaction(newTransaction);
        redirectAttributes.addFlashAttribute("loggedUser", userService.getUserById(newTransaction.getUser().getId()));
        return "redirect:/user/userPage";
    }

    @GetMapping("/deleteTransaction")
    public String deleteUser(@RequestParam("transactionId") int idFromTransactionToDelete,
                             @RequestParam("userId") int loggedUserId,
                             RedirectAttributes redirectAttributes){
        transactionService.deleteTransactionById(idFromTransactionToDelete);
        redirectAttributes.addFlashAttribute("loggedUser", userService.getUserById(loggedUserId));
        return "redirect:/user/userPage";
    }

    @GetMapping("/updateTransaction")
    public String updateUser(@RequestParam("transactionId") int idFromTransactionToUpdate,
                             @RequestParam("userId") int loggedUserId,
                             RedirectAttributes redirectAttributes,
                             Model theModel){
        Transaction transactionToUpdate = transactionService.getSingleTransactionById(idFromTransactionToUpdate);
        transactionToUpdate.setUser(userService.getUserById(loggedUserId));
        theModel.addAttribute("newTransaction",transactionToUpdate);
        theModel.addAttribute("category", categoryService.getAllCategories());
        redirectAttributes.addFlashAttribute("userId", loggedUserId);
        return "addTransaction";
    }

    private Date getFormattedDate(HttpServletRequest request) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String date = request.getParameter("transactionDate");
        LocalDate localDate = LocalDate.parse(date, formatter);
        return Date.from(localDate.atStartOfDay().toInstant(ZoneOffset.ofHours(-3)));
    }

}

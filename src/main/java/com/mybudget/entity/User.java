package com.mybudget.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name="users")
@NamedQueries({
        @NamedQuery(name="get_all_users",query="select users from User users")
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    @NotFound(action = NotFoundAction.IGNORE)
    private int id;
    @Column(name = "login")
    @NotNull(message = "is required")
    @Size(min = 1, message = "is required")
    private String login;
    @Column(name="email")
    @NotNull(message = "is required")
    @Size(min=1, message="is required")
    private String email;
    @Column(name = "password")
    @NotNull(message = "is required")
    @Size(min=6, message="is required")
    private String password;
    @Column(name = "join_date")
    @Temporal(TemporalType.TIMESTAMP)
    @Type(type="timestamp")
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private Date userJoinDate;
    public static final String ROLE = "REGISTERED";

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Transaction> userTransactionsList = new ArrayList<>();

    @OneToMany(mappedBy = "fixedTransactionUser", fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    private List<FixedTransaction> fixedTransactionsUserList = new ArrayList<>();

    public User(){}

    public User(String login, String password, Date userJoinDate) {
        this.login = login;
        this.password = password;
        this.userJoinDate = userJoinDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getUserJoinDate() {
        return userJoinDate;
    }

    public void setUserJoinDate(Date uderJoinDate) {
        this.userJoinDate = uderJoinDate;
    }

    public List<Transaction> getUserTransactionsList() {
        return userTransactionsList;
    }

    public void setUserTransactionsList(LinkedList<Transaction> userTransactionsList) {
        this.userTransactionsList = userTransactionsList;
    }

    public List<FixedTransaction> getFixedTransactionsUserList() {
        return fixedTransactionsUserList;
    }

    public void setFixedTransactionsUserList(List<FixedTransaction> fixedTransactionsUserList) {
        this.fixedTransactionsUserList = fixedTransactionsUserList;
    }

    public static String getROLE() {
        return ROLE;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", uderJoinDate=" + userJoinDate +
                '}';
    }
}

package com.mybudget.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "fixed_transaction")
@NamedQueries({
        @NamedQuery(name ="get_all_fixed_transactions", query = "select ft from FixedTransaction ft")
})
public class FixedTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_fixed_transaction")
    private int fixedTransactionId;

    @Column(name = "amount")
    @NotNull(message = "is required")
    private BigDecimal fixedMoneyAmount;

    @Temporal(TemporalType.DATE)
    @Column(name = "begin_date")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date durationBeginDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date durationEndDate;

    @Column(name = "comment")
    private String transactionComment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user")
    private User fixedTransactionUser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_category")
    private Category category;

    public FixedTransaction(){}

    public FixedTransaction(BigDecimal fixedMoneyAmount, Date durationBeginDate, Date durationEndDate, String transactionComment) {
        this.fixedMoneyAmount = fixedMoneyAmount;
        this.durationBeginDate = durationBeginDate;
        this.durationEndDate = durationEndDate;
        this.transactionComment = transactionComment;
    }

    public int getFixedTransactionId() {
        return fixedTransactionId;
    }

    public void setFixedTransactionId(int fixedTransactionId) {
        this.fixedTransactionId = fixedTransactionId;
    }

    public BigDecimal getFixedMoneyAmount() {
        return fixedMoneyAmount;
    }

    public void setFixedMoneyAmount(BigDecimal fixedMoneyAmount) {
        this.fixedMoneyAmount = fixedMoneyAmount;
    }

    public Date getDurationBeginDate() {
        return durationBeginDate;
    }

    public void setDurationBeginDate(Date durationBeginDate) {
        this.durationBeginDate = durationBeginDate;
    }

    public Date getDurationEndDate() {
        return durationEndDate;
    }

    public void setDurationEndDate(Date durationEndDate) {
        this.durationEndDate = durationEndDate;
    }

    public String getTransactionComment() {
        return transactionComment;
    }

    public void setTransactionComment(String transactionComment) {
        this.transactionComment = transactionComment;
    }

    public User getFixedTransactionUser() {
        return fixedTransactionUser;
    }

    public void setFixedTransactionUser(User fixedTransactionUser) {
        this.fixedTransactionUser = fixedTransactionUser;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category fixedTransactionsInCategory) {
        this.category = fixedTransactionsInCategory;
    }

    @Override
    public String toString() {
        return "FixedTransaction{" +
                "fixedTransactionId=" + fixedTransactionId +
                ", fixedMoneyAmount=" + fixedMoneyAmount +
                ", durationBeginDate=" + durationBeginDate +
                ", durationEndDate=" + durationEndDate +
                ", transactionComment='" + transactionComment + '\'' +
                '}';
    }
}


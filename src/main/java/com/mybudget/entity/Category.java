package com.mybudget.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "category")
@NamedQueries(value= {
        @NamedQuery(name="get_all_categories", query="select category from Category category"),
        @NamedQuery(name="get_all_expenditure_categories", query="select category from Category category where category.isExpenditure = true")
})
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_category")
    private int id;
    @Column(name="category_name")
    private String categoryName;
    @Column(name = "is_expenditure")
    private boolean isExpenditure;
    @NotFound(action = NotFoundAction.IGNORE)
    @OneToMany(mappedBy = "category", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    List<Transaction> listOfTransactionsInCategory = new ArrayList<>();
    @NotFound(action = NotFoundAction.IGNORE)
    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    List<FixedTransaction> listOfFixedTransactions = new ArrayList<>();

    public Category(){}

    public Category(String categoryName, boolean isExpenditure) {
        this.categoryName = categoryName;
        this.isExpenditure = isExpenditure;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isExpenditure() {
        return isExpenditure;
    }

    public void setIsExpenditure(boolean isExpenditure) {
        this.isExpenditure = isExpenditure;
    }

    public List<Transaction> getListOfTransactionsInCategory() {
        return listOfTransactionsInCategory;
    }

    public void setListOfTransactionsInCategory(LinkedList<Transaction> userTransactionsList) {
        this.listOfTransactionsInCategory = userTransactionsList;
    }

    public List<FixedTransaction> getListOfFixedTransactions() {
        return listOfFixedTransactions;
    }

    public void setListOfFixedTransactions(List<FixedTransaction> listOfFixedTransactions) {
        this.listOfFixedTransactions = listOfFixedTransactions;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", categoryName='" + categoryName + '\'' +
                ", isExpenditure=" + isExpenditure +
                '}';
    }
}

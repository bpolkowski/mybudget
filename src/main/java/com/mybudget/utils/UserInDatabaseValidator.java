package com.mybudget.utils;

import com.mybudget.entity.User;
import com.mybudget.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserInDatabaseValidator {

    @Autowired
    private UserService userService;

    public boolean isUserInDatabase(User userToCheck){
        List<User> usersInDatabase = userService.getAllUsers();
        for (User userInDb : usersInDatabase) {
            if((userInDb.getLogin().equals(userToCheck.getLogin()))&&
            userInDb.getPassword().equals(userToCheck.getPassword())){
                return true;
            }
        }
        return false;
    }

    public User getUserEntityFromDatabase(User userToGet){
        return userService.getUserByLoginAndPassword(userToGet.getLogin(),userToGet.getPassword());
    }

}

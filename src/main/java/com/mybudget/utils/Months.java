package com.mybudget.utils;

public enum Months {
    JANUARY(1, "January"),
    FEBRUARY(2, "February"),
    MARCH(3, "March"),
    APRIL(4, "April"),
    MAY(5, "May"),
    JUNE(6, "June"),
    JULY(7, "July"),
    AUGUST(8, "August"),
    SEPTEMBER(9, "September"),
    OCTOBER(10, "October"),
    NOVEMBER(11, "November"),
    DECEMBER(12, "December");

    private int monthId;
    private String monthName;

    Months(int monthId, String monthName) {
        this.monthId = monthId;
        this.monthName = monthName;
    }

    public static String getById(int id){
        for(Months month : Months.values()){
            if(month.monthId == id)
                return month.monthName;
        }
        return null;
    }


}

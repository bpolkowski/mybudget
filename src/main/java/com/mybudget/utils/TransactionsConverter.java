package com.mybudget.utils;

import com.mybudget.entity.FixedTransaction;
import com.mybudget.entity.Transaction;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class TransactionsConverter {

    @PersistenceContext
    EntityManager entityManager;

    private HashMap<String, List<Transaction>> transactionsPerMonth = new HashMap<>();

    private HashMap<String, List<FixedTransaction>> fixedTransactionsPerMonth = new HashMap<>();

    public HashMap<String,List<Transaction>> convertTransactionsPerMonth(int userId){
        for(int monthId = 1; monthId < 13; monthId++){
            List<Transaction> transactionsFromDatabase = new ArrayList<>(entityManager
                .createQuery("SELECT t FROM Transaction t WHERE MONTH(t.transactionDate) = :monthId AND t.user.id = :userId", Transaction.class)
                .setParameter("monthId", monthId)
                .setParameter("userId", userId)
                .getResultList());
        transactionsPerMonth.put(Months.getById(monthId), transactionsFromDatabase);
        }
        return transactionsPerMonth;
    }

    public List<Transaction> transactionsInMonth(String month){
        return transactionsPerMonth.get(month);
    }

    public HashMap<String,List<FixedTransaction>> convertFixedTransactionsPerMonth(int userId){
        for(int monthId = 1; monthId < 13; monthId++){
            List<FixedTransaction> fixedTransactionsFromDatabase = new ArrayList<>(entityManager
                    .createQuery("SELECT ft FROM FixedTransaction ft WHERE MONTH(ft.durationBeginDate) = :monthId AND ft.fixedTransactionUser.id = :userId", FixedTransaction.class)
                    .setParameter("monthId", monthId)
                    .setParameter("userId", userId)
                    .getResultList());
            fixedTransactionsPerMonth.put(Months.getById(monthId), fixedTransactionsFromDatabase);
        }
        return fixedTransactionsPerMonth;
    }

    public List<FixedTransaction> fixedTransactionsInMonth(String month){
        return fixedTransactionsPerMonth.get(month);
    }
}